# docker-pi

Just a simple collection of steps to install docker on Pi running Raspberry OS. 

# Taken from 

https://phoenixnap.com/kb/docker-on-raspberry-pi



## Steps 

### 1.) Run 
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

### 2.) Set to your raspberry user 

```
sudo usermod -aG docker [user]
```
### 3.) Check
```
docker version
```

### 4.) Test
```
docker run hello-world
```
### 5.) Install docker-compose

```
sudo apt-get -y install python-pip
sudo pip install docker-compose
```